# coding=UTF-8
"""Fetch the forecast from forecast.io and display a summary

Notes from https://developer.forecast.io/docs/v2:

precipIntensity: A numerical value representing the average expected
intensity (in inches of liquid water per hour) of precipitation
occurring at the given time conditional on probability (that is,
assuming any precipitation occurs at all). A very rough guide is that a
value of 0 in./hr. corresponds to no precipitation, 0.002 in./hr.
corresponds to very light precipitation, 0.017 in./hr. corresponds to
light precipitation, 0.1 in./hr. corresponds to moderate precipitation,
and 0.4 in./hr. corresponds to heavy precipitation.

apparentTemperature (not defined on daily data points): A numerical
value representing the apparent (or “feels like”) temperature at the
given time in degrees Fahrenheit.

"""

import argparse
import json

import tornado.gen
import tornado.httpclient
import tornado.ioloop
import tornado.log

import unicornhat

LOG = tornado.log.app_log

URL_TEMPLATE = "https://api.forecast.io/forecast/{api_key}/{latitude},{longitude}?units={units}"

PRECIPITATION_BANDS = (0.002, 0.01, 0.1, 0.4)
PRECIPITATION_COLOURS = (
    (0x81, 0xB3, 0xEE),
    (0x81, 0xB3, 0xEE),
    (0x50, 0x7C, 0xC5),
    (0x35, 0x47, 0x8C),
)

TEMPERATURE_BANDS = (0, 5, 10, 15)
TEMPERATURE_COLOURS = (
    (0x37, 0x5D, 0x81),
    (0xF3, 0xE4, 0xA1),
    (0xFD, 0xAF, 0x48),
    (0xB4, 0x49, 0x2C),
)


def display_weather(data):
    unicornhat.clear()

    for x, hour in enumerate(data["hourly"]["data"][:8]):
        LOG.info("Processing hour {}/{}".format(x, hour))
        precipitation = hour["precipIntensity"]
        draw_bar(x, 4, precipitation, PRECIPITATION_BANDS, PRECIPITATION_COLOURS)
        temperature = hour["apparentTemperature"]
        draw_bar(x, 0, temperature, TEMPERATURE_BANDS, TEMPERATURE_COLOURS)

    unicornhat.show()


def draw_line(x, y, value, bands, pallete):
    pixel = None
    for i, (band_min, colour) in enumerate(zip(bands, pallete)):
        if value >= band_min:
            pixel = (7 - x, y + i, colour)

    if pixel:
        # origin is at bottom right, so mirror the x axis
        x, y, colour = pixel
        unicornhat.set_pixel(x, y, *colour)


def draw_bar(x, y, value, bands, pallete):
    for i, (band_min, colour) in enumerate(zip(bands, pallete)):
        if value >= band_min:
            # origin is at bottom right, so mirror the x axis
            unicornhat.set_pixel(7 - x, y + i, *colour)


@tornado.gen.coroutine
def fetch_and_process_weather(url):
    LOG.info("Fetching {}".format(url))
    http_client = tornado.httpclient.AsyncHTTPClient()
    response = yield http_client.fetch(url, request_timeout=30)
    LOG.info(response)
    data = json.load(response.buffer)
    LOG.info("Processing data ({} hours of data)".format(len(data["hourly"]["data"])))
    display_weather(data)


def fetch_weather_tick(url, delay):
    ioloop = tornado.ioloop.IOLoop.instance()
    fetch_and_process_weather(url)
    LOG.info("Scheduling next call in {} seconds".format(delay))
    ioloop.call_later(delay, fetch_weather_tick, url, delay)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--units", default="si")
    parser.add_argument("--delay", default=600, type=int)
    parser.add_argument("api_key")
    parser.add_argument("latitude")
    parser.add_argument("longitude")
    args = parser.parse_args()

    url = URL_TEMPLATE.format(
        api_key=args.api_key,
        latitude=args.latitude,
        longitude=args.longitude,
        units=args.units
    )

    tornado.log.enable_pretty_logging()
    calls_per_day = (24 * 60 * 60) / args.delay
    LOG.info("Starting up, using delay {} between fetches (= {} calls per day)".format(args.delay, calls_per_day))
    unicornhat.brightness(0.05)
    ioloop = tornado.ioloop.IOLoop.instance()
    fetch_weather_tick(url, args.delay)
    ioloop.start()


if __name__ == '__main__':
    main()
