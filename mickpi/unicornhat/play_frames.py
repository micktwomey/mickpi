import argparse
import logging

import time

from PIL import Image
from PIL import ImageOps

import unicornhat

LOG = logging.getLogger(__name__)


def slice_image(width, height, offset=8):
    """Slices image into frames

    Walks left to right, top to bottom.

    """
    x = 0
    y = 0
    while True:
        yield (x, y)
        x += offset
        if x >= width:
            x = 0
            y += offset
        if y >= height:
            y = 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--fps", default=10, type=int)
    parser.add_argument("--brightness", default=0.05, type=float)
    parser.add_argument("--mirror", default=False, action="store_true")
    parser.add_argument("--rotation", default=270, type=int)
    parser.add_argument("--debug", default=False, action="store_true")
    parser.add_argument("file", type=argparse.FileType('rb'), default="-")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            format="%(asctime)s %(levelname)s %(filename)s:%(lineno)d %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
            level=logging.DEBUG
        )
    else:
        logging.basicConfig(format="%(message)s", level=logging.INFO)

    LOG.info("Playing {}".format(args.file))

    image = Image.open(args.file)
    LOG.debug(image)

    image = image.convert("RGB")

    if args.mirror:
        image = ImageOps.mirror(image)

    unicornhat.brightness(args.brightness)
    unicornhat.rotation(args.rotation)
    pixels = unicornhat.get_pixels()

    LOG.debug(image)
    width, height = image.size

    for x_offset, y_offset in slice_image(width, height):
        LOG.debug((x_offset, y_offset))
        for x in range(8):
            for y in range(8):
                pixels[x][y] = image.getpixel((x + x_offset, y + y_offset))[:3]
        unicornhat.set_pixels(pixels)
        unicornhat.show()
        time.sleep(1.0 / args.fps)

if __name__ == '__main__':
    main()
