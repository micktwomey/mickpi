"""Adafruit Character LCD Plate

Their funky 16x2 display

"""

import argparse
import sys
import time

from .Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input",
        type=argparse.FileType("rb"),
        default=sys.stdin,
        help="Filename to read from (- for stdin)",
    )
    args = parser.parse_args()

    lcd = Adafruit_CharLCDPlate()
    try:
        lcd.begin(16, 2)
        lcd.clear()
        for line in args.input:
            lcd.message(line)
            time.sleep(1)
    finally:
        lcd.clear()
        lcd.stop()


if __name__ == "__main__":
    main()

