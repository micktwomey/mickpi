from __future__ import absolute_import
"""Laika Explorer

Currently requires laika module installed from
https://github.com/eightdog/laika.git

Plan is to first wrap directly using cffi (cleaner) and then move to simply
implementing directly in cffi (even cleaner).

"""

from laika import lk
from laika.explorer import exp

BUTTON_MASKS = {
    0: exp.BUTTON_0_MASK,
    1: exp.BUTTON_1_MASK,
    2: exp.BUTTON_2_MASK,
    3: exp.BUTTON_3_MASK,
}


class LaikaExplorer(object):
    def __init__(self):
        self.buffer = [None] * 32
        self.module = lk.MODULE_ONE

    def din(self):
        exp.din(self.module, self.buffer)
        return self.buffer[0]

    def read_button(self, button):
        """Reads a button state

        :param button: 0-3
        :returns: True if button is pressed.

        """
        return (self.din() & BUTTON_MASKS[button]) == 0

    def read_buttons(self):
        return [self.read_button(i) for i in range(4)]

    @property
    def buttons(self):
        return self.read_buttons()

    @property
    def button0(self):
        return self.read_button(0)

    @property
    def button1(self):
        return self.read_button(1)

    @property
    def button2(self):
        return self.read_button(2)

    @property
    def button3(self):
        return self.read_button(3)
    
    def ain(self):
        exp.ain(self.module, self.buffer)
        return self.buffer[:2]

    def read_analog(self, analog_input):
        return self.ain()[analog_input]

    @property
    def analogs(self):
        return self.ain()

    @property
    def adc0(self):
        return read_analog(0)

    @property
    def adc1(self):
        return read_analog(1)

    def __enter__(self):
        lk.init()
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        lk.exit()


if __name__ == "__main__":
    import time
    with LaikaExplorer() as le:
        print("Press button 0 to exit")
        while not le.button0:
            print(("buttons", le.buttons))
            print(("analogs", le.analogs))
            time.sleep(0.1)
        print("Button 0 pressed!")

